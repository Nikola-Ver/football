export interface Match {
    id?: number,
    header: string,
    result: string,
    hasStatistics: Boolean,
    hasImg: Boolean,
    statistics?: JSON,
    img?: Buffer
}
