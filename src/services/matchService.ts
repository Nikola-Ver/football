import { Match } from '../models/Match';
import { dbService } from './dbService';
import { s3Service } from './s3Service';

class MatchService {
  async saveMatch(match: Match): Promise<void> {
    if (match.img) {
      await s3Service.saveImg(String(match.id), match.img);
      delete match.img;
      match.hasImg = true;
    }

    if (match.statistics) {
      await s3Service.saveStatistics(String(match.id), match.statistics);
      delete match.statistics;
      match.hasStatistics = true;
    }
    
    await dbService.saveRecord(match);
  }

  async getMatches(): Promise<Match[] | null> {
    let records = await dbService.getRecords();

    if (records)
      for (let i = 0; i < records.length; ++i) {
        if (records[i].hasImg) {
          records[i].img = await s3Service.getImg(String(records[i].id));
        }

        if (records[i].hasStatistics) {
          records[i].statistics = await s3Service.getStatistics(
            String(records[i].id)
          );
        }
      }
    return records;
  }

  async getMatch(id: number): Promise<Match | null> {
    let record = await dbService.getRecord(id);

    if (record?.hasImg) {
      record.img = await s3Service.getImg(String(record.id));
    }

    if (record?.hasStatistics) {
      record.statistics = await s3Service.getStatistics(String(record.id));
    }

    return record;
  }

  closeDbConnection(): void {
    dbService.closeConnection();
  }
}

export const matchService = new MatchService();
