import { Match } from '../models/Match';
import mysql from 'mysql';
import dotenv from 'dotenv';

dotenv.config();

const SQL_GET_BY_ID: string = 'SELECT * FROM matches WHERE id = ?';
const SQL_GET_ALL: string = 'SELECT * FROM matches';
const SQL_INSERT: string = 'INSERT INTO matches SET ?';

class DbService {
  private connection: mysql.Connection;

  constructor() {
    this.connection = mysql.createConnection({
      host: process.env.DB_HOST,
      user: process.env.DB_USER,
      password: process.env.DB_PASS,
      database: process.env.DB_NAME,
    });

    this.connection.connect(function (err) {
      if (err) throw err;
    });
  }

  async closeConnection(): Promise<void> {
    return new Promise((res, rej) => {
      this.connection.end(() => {
        res();
      });
    });
  }

  async getRecord(id: number): Promise<Match | null> {
    return new Promise((res, rej) => {
      this.connection.query(
        SQL_GET_BY_ID,
        [id],
        (err, result: Match, fields) => {
          if (err) throw err;
          res(result);
        }
      );
    });
  }

  async getRecords(): Promise<Match[] | null> {
    return new Promise((res, rej) => {
      this.connection.query(SQL_GET_ALL, (err, results: Match[], fields) => {
        if (err) throw err;
        res(results);
      });
    });
  }

  async saveRecord(match: Match): Promise<void> {
    return new Promise((res, rej) => {
      this.connection.query(SQL_INSERT, match, (err, results, fields) => {
        if (err) throw err;
        res();
      });
    });
  }
}

export const dbService = new DbService();
