import express, { NextFunction, Response, Request } from 'express';
import { matchService } from '../services/matchService';
import { Match } from '../models/Match';
import { UploadedFile } from 'express-fileupload';

export const closeDbConnection = matchService.closeDbConnection;
export const router = express.Router();

router
  .get('/', async (req, res) => {
    const result = await matchService.getMatches();
    res.send({ result });
  })

  .get('/:id', async (req, res) => {
    const id = Number(req.query.id);
    const result = await matchService.getMatch(id);
    res.send({ result });
  })

  .post('/', async (req, res) => {
    let match: Match = {
      header: req.body.header,
      result: req.body.result,
      statistics: req.body.statistics, 
      img: (req.files?.img as UploadedFile).data,
      hasStatistics: false,
      hasImg: false
    };

    await matchService.saveMatch(match)
    res.send();
  })

  .use((err: Error, req: Request, res: Response, next: NextFunction) => {
    res.status(500).send({ err });
  });
