import AWS from 'aws-sdk';
import dotenv from 'dotenv';

dotenv.config();

const BASE_64 = 'base64';

class S3Service {
  private s3: AWS.S3;

  constructor() {
    this.s3 = new AWS.S3({
      accessKeyId: process.env.AWS_ID,
      secretAccessKey: process.env.AWS_SECRET,
    });
  }

  async getImg(id: string): Promise<Buffer> {
    return new Promise((res, rej) => {
      const params: AWS.S3.GetObjectRequest = {
        Bucket: process.env.AWS_BUCKET_NAME!,
        Key: `${id}Img`,
      };

      this.s3.getObject(params, (err, data) => {
        res((Buffer.from(data.Body as string, BASE_64) as unknown) as Buffer);
      });
    });
  }

  async saveImg(id: string, img: Buffer): Promise<void> {
    return new Promise((res, rej) => {
      const params: AWS.S3.PutObjectRequest = {
        Bucket: process.env.AWS_BUCKET_NAME!,
        Key: `${id}Img`,
        Body: Buffer.from(img).toString(BASE_64),
      };

      this.s3.upload(params, (err: unknown, data: unknown) => {
        if (err) throw err;
        res();
      });
    });
  }

  async getStatistics(id: string): Promise<JSON> {
    return new Promise((res, rej) => {
      const params: AWS.S3.GetObjectRequest = {
        Bucket: process.env.AWS_BUCKET_NAME!,
        Key: `${id}Statistics`,
      };

      this.s3.getObject(params, (err, data) => {
        if (err) throw err;
        res((data.Body?.toString() as unknown) as JSON);
      });
    });
  }

  async saveStatistics(id: string, statistics: JSON): Promise<void> {
    return new Promise((res, rej) => {
      const params: AWS.S3.PutObjectRequest = {
        Bucket: process.env.AWS_BUCKET_NAME!,
        Key: `${id}Statistics`,
        Body: statistics,
      };

      this.s3.upload(params, (err: unknown, data: unknown) => {
        if (err) throw err;
        res();
      });
    });
  }
}

export const s3Service = new S3Service();
