import express from 'express';
import fileupload from 'express-fileupload';
import { router, closeDbConnection } from './routes/router';

const PORT = 4000;

const app = express();

const server = app
  .use(express.json())

  .use(fileupload())

  .use('/', router)

  .listen(PORT);

server.on('close', () => {
  closeDbConnection();
});

process.on('SIGINT', () => {
  server.close();
});
